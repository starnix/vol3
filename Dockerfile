FROM alpine:latest
MAINTAINER "David Rose david.rose@starnix.se"
RUN apk add --no-cache python3 py3-pip py3-appdirs git make gcc g++ python3-dev musl-dev libusb-dev capstone snappy-dev &&\
    git clone https://github.com/volatilityfoundation/volatility3 /vol3 &&\
    pip3 install -r /vol3/requirements.txt &&\
    apk del py3-pip py3-appdirs python3-dev git make gcc g++ musl-dev libusb-dev snappy-dev &&\
    mkdir /files &&\
    rm -Rf /vol3/.git &&\
    rm -Rf /vol3/.github &&\
    rm -Rf /vol3/.readthedocs.yml

VOLUME /files

WORKDIR /files

ENTRYPOINT ["python3", "/vol3/vol.py"]



